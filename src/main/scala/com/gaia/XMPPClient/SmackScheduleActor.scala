package com.gaia.XMPPClient

import akka.actor.{Props, ActorLogging, Actor}
import akka.routing.FromConfig
import org.jivesoftware.smack.util.{ActorStopSchedule, ActorSchedule}
import org.joda.time.DateTime
import scala.concurrent.duration._
/**
 * Created by ktz on 15. 12. 21.
 */
class SmackScheduleActor extends Actor with ActorLogging{
  case class RequestToRunTask(uuid : String, runnable: Runnable)
  case object PingPlease
  val scheduledWorks : scala.collection.mutable.Map[String, Runnable] = scala.collection.mutable.Map.empty
  context.system.scheduler.schedule((3 minute) + (30 second), (3 minute) + (30 second), self, PingPlease)(context.dispatcher, self)
  private val mBalancedRouter = context.actorOf(FromConfig.props(Props[SmackActor]), "pingRouter")
  def receive = {
    case actorSchedule : ActorSchedule =>
//      scheduledWorks += actorSchedule.uuid -> context.system.scheduler.schedule(actorSchedule.timeout millisecond, actorSchedule.timeout millisecond, self, RequestToRunTask(actorSchedule.uuid, actorSchedule.runnable))(context.dispatcher)
      scheduledWorks += actorSchedule.uuid -> actorSchedule.runnable
    case PingPlease =>
      log.info(s"Request Ping")
      val startTime = DateTime.now
      scheduledWorks.par.foreach(mBalancedRouter ! _._2)
      log.info(s"Ping End : ${DateTime.now.getMillis - startTime.getMillis} millis")
    case stopSchedule : ActorStopSchedule =>
      scheduledWorks -= stopSchedule.uuid
  }
}