package com.gaia.XMPPClient.Result

import akka.actor.{ActorRef, Props, Actor}
import com.gaia.XMPPClient.ManagementMessage.{MsgEnvelope, SlaveState, RequestSlaveState, ActorStatic}
import com.gaia.XMPPClient.XMPPActor.RequestToReconnect
import com.gaia.XMPPClient.XMPPActor.XMPPWrapper.{ConnectionAuthenticated, ConnectionClosed, ConnectionClosedOnError, ConnectionConnected}
import scala.concurrent.duration._
import scala.collection.mutable.ListBuffer
import scala.util.Random

/**
 * Created by ktz on 15. 10. 23.
 */
class StaticSelfActor(mID : String, nOfClient : Int) extends StaticManager with Actor{
  (0 until nOfClient) foreach {i =>
    mXMPPClientStatic += s"${mID}_$i" -> ActorStatic(false, false, 0, 0)
  }
  val xmppClientActorList : ListBuffer[ActorRef] = ListBuffer.empty
  var reconnectStarted = false

  def receive = {
    case ConnectionConnected(mConnection) =>
      val static = mXMPPClientStatic(sender().path.name)
      reviseStatic(ActorStatic(true, false, static.nOfReconnect, static.nOfFailure), sender().path.name)
      if(!xmppClientActorList.exists(_.path.name == sender().path.name)) {
        xmppClientActorList += sender()
        if(xmppClientActorList.size > nOfClient * 0.9 && !reconnectStarted) {
          reconnectStarted = true
          import context.dispatcher
          context.system.scheduler.schedule(1 second, 1 second) {
            (for{
              i <- 0 until 40
            } yield Random.nextInt(xmppClientActorList.size)).distinct.toList.foreach{i =>
              xmppClientActorList(i) ! MsgEnvelope(self, RequestToReconnect())
            }
          }
        }
      }
    case ConnectionAuthenticated(mConnection, resumed) =>
      val static = mXMPPClientStatic(sender().path.name)
      reviseStatic(ActorStatic(true, false, static.nOfReconnect, static.nOfFailure), sender().path.name)
      if(!xmppClientActorList.exists(_.path.name == sender().path.name)) xmppClientActorList += sender()
    case ConnectionClosedOnError(e) =>
      xmppClientActorList -= sender()
      val static = mXMPPClientStatic(sender().path.name)
      reviseStatic(ActorStatic(false, true, static.nOfReconnect +1, static.nOfFailure + 1), sender().path.name)
    case ConnectionClosed() =>
      val static = mXMPPClientStatic(sender().path.name)
      reviseStatic(ActorStatic(false, false, static.nOfReconnect, static.nOfFailure), sender().path.name)
    case RequestSlaveState() =>
      sender() ! SlaveState(mID, mXMPPClientStatic.count(_._2.connected == true),
        mXMPPClientStatic.count(_._2.connected == true),
        mXMPPClientStatic.count(_._2.reconnecting),
        mXMPPClientStatic.map(_._2.nOfReconnect).sum,
        mXMPPClientStatic.map(_._2.nOfFailure).sum)
  }

  def reviseStatic(static : ActorStatic, key : String): Unit ={
    mXMPPClientStatic.remove(key)
    mXMPPClientStatic += key -> static
  }
}

object StaticSelfActor {
  def apply(mID : String, nOfClient : Int) = Props(new StaticSelfActor(mID, nOfClient))
}
