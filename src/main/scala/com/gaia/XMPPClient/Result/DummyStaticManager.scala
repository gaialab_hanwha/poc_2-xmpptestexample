package com.gaia.XMPPClient.Result

import akka.actor.{Props, Actor}

/**
 * Created by ktz on 15. 10. 14.
 */
class DummyStaticManager extends Actor{
  def receive = {
    case _=>
  }
}

object DummyStaticManager {
  def props : Props = Props[DummyStaticManager]
}