package com.gaia.XMPPClient


import java.util.concurrent.{LinkedBlockingDeque, TimeUnit, ThreadPoolExecutor}

import akka.actor._
import akka.util.Timeout
import org.jivesoftware.smack.util.ActorFactory
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Await}
import akka.pattern.ask
/**
 * Created by ktz on 15. 12. 18.
 */
class SmackActorFactory(actorSystem: ActorSystem) extends ActorFactory {
  case class requestMakeActor(props : Props, actorName : String)
  val mSmackParent = actorSystem.actorOf(Props(new Actor {
    def receive = {
      case requestMakeActor(props, name) =>
        sender ! context.actorOf(props, name)
    }
  }), "SmackActorParent")
  private implicit val timeout = Timeout(10 second)
  val mSmackActorMap : Map[String, ActorRef] = Map(
//    "PingManager" -> Await.result(mSmackParent.ask(requestMakeActor(Props[SmackScheduleActor], "PingManager")), 10 second).asInstanceOf[ActorRef],
    "executorService" -> Await.result(mSmackParent.ask(requestMakeActor(Props[SmackActor], "executorService")), 10 second).asInstanceOf[ActorRef],
    "removeCallbacksService" -> Await.result(mSmackParent.ask(requestMakeActor(Props[SmackActor], "removeCallbacksService")), 10 second).asInstanceOf[ActorRef],
    "singleThreadedExecutorService" -> Await.result(mSmackParent.ask(requestMakeActor(Props[SmackActor], "singleThreadedExecutorService")), 10 second).asInstanceOf[ActorRef])

  override def getExecutorActor(actorName : String): ActorRef = {
    mSmackActorMap(actorName)
  }
}

object SmackActorFactory {
  val smackExecutionContext = ExecutionContext.fromExecutor(new ThreadPoolExecutor(50, 1024, 1000, TimeUnit.MICROSECONDS, new LinkedBlockingDeque[Runnable]()))
}