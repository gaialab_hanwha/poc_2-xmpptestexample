package com.gaia.XMPPClient

import akka.actor.Actor.Receive
import akka.actor.{Actor, Props, ActorSystem}
import com.typesafe.config.ConfigFactory
import org.jivesoftware.smack.util.ActorWrapper

/**
 * Created by ktz on 15. 7. 17.
 */
object Main extends App {
  val system = ActorSystem("testSystem")
  ActorWrapper.mActorFactory = new SmackActorFactory(system)
  val client = system.actorOf(Props(new ActorClient(s"masterSystem@${ConfigFactory.load("application").getString("akka.master")}", "MasterActor")).withDispatcher("onlyFor-ActorClient-dispatcher"), "ActorClient")
}