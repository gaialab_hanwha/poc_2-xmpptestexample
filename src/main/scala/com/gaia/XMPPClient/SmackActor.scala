package com.gaia.XMPPClient



import akka.actor.{ActorLogging, Cancellable, Actor}
import org.jivesoftware.smack.util.{ActorStopSchedule, ActorSchedule}
import scala.concurrent.duration._
import scala.concurrent.Future

/**
 * Created by ktz on 15. 12. 18.
 */
class SmackActor extends Actor with ActorLogging{
  def receive = {
    case runnable : Runnable =>
      Future{
        runnable.run()
      }(SmackActorFactory.smackExecutionContext)
    case _=>
  }
}