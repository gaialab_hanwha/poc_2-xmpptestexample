package com.gaia.XMPPClient

import akka.actor.ActorSystem
import akka.dispatch.{PriorityGenerator, UnboundedPriorityMailbox}
import com.gaia.XMPPClient.ManagementMessage.RequestSlaveState
import com.gaia.XMPPClient.XMPPActor.{MessageReceived, caseSendMessage}
import com.typesafe.config.Config

/**
 * Created by ktz on 15. 10. 23.
 */
class PriorityMailBox(settings: ActorSystem.Settings, config: Config) extends UnboundedPriorityMailbox(PriorityGenerator{
  case RequestSlaveState() => 1
  case caseSendMessage(chat, message) => 2
  case MessageReceived(chat, message) => 2
  case _=> 3
})