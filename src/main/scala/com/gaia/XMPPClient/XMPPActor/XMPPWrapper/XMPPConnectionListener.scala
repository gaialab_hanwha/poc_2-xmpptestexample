package com.gaia.XMPPClient.XMPPActor.XMPPWrapper

import akka.actor.Actor.Receive
import akka.actor.{Actor, ActorRef}
import org.jivesoftware.smack.tcp.XMPPTCPConnection
import org.jivesoftware.smack.{XMPPConnection, ConnectionListener}

import scala.collection.mutable.ListBuffer
import scala.concurrent.{Future, Promise}

/**
 * Created by ktz on 15. 8. 17.
 */
trait ConnectionResult
case class ConnectionConnected(connection : XMPPConnection) extends ConnectionResult
case class ConnectionReconnectionFailed(e : Exception) extends ConnectionResult
case class ConnectionReconnectionSuccessful() extends ConnectionResult
case class ConnectionAuthenticated(connection: XMPPConnection, resumed: Boolean) extends ConnectionResult
case class ConnectionClosedOnError(e : Exception) extends ConnectionResult
case class ConnectionClosed() extends ConnectionResult
case class ConnectionReconnectingIn(second : Int) extends ConnectionResult

class XMPPConnectionListener(listenerActor : ActorRef, sender : ActorRef) extends ConnectionListener{

  override def connected(connection: XMPPConnection): Unit = listenerActor.tell(ConnectionConnected(connection), sender)

  override def reconnectionFailed(e: Exception): Unit = listenerActor.tell(ConnectionReconnectionFailed(e), sender)

  override def reconnectionSuccessful(): Unit = listenerActor.tell(ConnectionReconnectionSuccessful(), sender)

  override def authenticated(connection: XMPPConnection, resumed: Boolean): Unit = listenerActor.tell(ConnectionAuthenticated(connection, resumed), sender)

  override def connectionClosedOnError(e: Exception): Unit = listenerActor.tell(ConnectionClosedOnError(e), sender)

  override def connectionClosed(): Unit = listenerActor.tell(ConnectionClosed(), sender)

  override def reconnectingIn(seconds: Int): Unit = listenerActor.tell(ConnectionReconnectingIn(seconds), sender)
}

object XMPPConnectionListener{
  def apply(listenerActor : ActorRef, sender : ActorRef) = new XMPPConnectionListener(listenerActor, sender)
}