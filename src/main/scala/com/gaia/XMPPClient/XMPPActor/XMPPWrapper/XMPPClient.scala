package com.gaia.XMPPClient.XMPPActor.XMPPWrapper

import javax.net.ssl.SSLContext

import com.typesafe.config.ConfigFactory
import org.jivesoftware.smack.ReconnectionManager.ReconnectionPolicy
import org.jivesoftware.smack.SmackException.{NotLoggedInException, NotConnectedException, AlreadyConnectedException, AlreadyLoggedInException}
import org.jivesoftware.smack.XMPPException.{XMPPErrorException}
import org.jivesoftware.smack._
import org.jivesoftware.smack.bosh.{BOSHConfiguration, XMPPBOSHConnection}
import org.jivesoftware.smack.chat.{ChatMessageListener, Chat, ChatManager}
import org.jivesoftware.smack.packet.Presence
import org.jivesoftware.smack.packet.XMPPError.Condition
import org.jivesoftware.smack.roster.{Roster, RosterEntry}
import org.jivesoftware.smack.tcp.{XMPPTCPConnection, XMPPTCPConnectionConfiguration}
import org.jivesoftware.smack.util.TLSUtils
import org.jivesoftware.smackx.iqregister.AccountManager
import scala.collection.JavaConversions._
import scala.util.Random
import scalaz.effect.IO

/**
 * Created by ktz on 15. 7. 15.
 */

trait XMPPClient {
  def GetConnection(serverName : String, hostName : String, bosh : Boolean) : AbstractXMPPConnection = {
    val config = CreateConfig(serverName, hostName, bosh)
    if(bosh) new XMPPBOSHConnection(config.asInstanceOf[BOSHConfiguration]) else new XMPPTCPConnection(config.asInstanceOf[XMPPTCPConnectionConfiguration])
  }

  def ConnectToServer(mConnection : AbstractXMPPConnection) : Either[Exception, Boolean] = try{
    if(!mConnection.isConnected) mConnection.connect()
    Right(true)
  }catch {
    case alreadyConnectedException :  AlreadyConnectedException =>
      Right(false)
    case exception : Exception =>
      Left(exception)
  }

  def CreateConfig[A](serverName : String, hostName : String, bosh : Boolean) : ConnectionConfiguration = {
    val builder = if(bosh) BOSHConfiguration.builder() else XMPPTCPConnectionConfiguration.builder()
    val configFactory = ConfigFactory.load("application")
    builder
      .setHost(serverName)
      .setServiceName(hostName)
    val tls_enabled = configFactory.getBoolean("test.security.tls-enable")
    if(tls_enabled) builder.setSecurityMode(ConnectionConfiguration.SecurityMode.required)
    builder match {
      case boshBuilder : BOSHConfiguration.Builder =>
        boshBuilder.setPort(5280)
        boshBuilder.setFile("/http-bind/")
      case myBuilder : XMPPTCPConnectionConfiguration.Builder =>
        myBuilder.setConnectTimeout(configFactory.getInt("test.interval.connection-timeout"))
        if(tls_enabled){
          val sslContext = SSLContext.getInstance("TLS")
          sslContext.getServerSessionContext.setSessionTimeout(1)
          sslContext.getServerSessionContext.setSessionCacheSize(1)
          sslContext.init(null, Array(new DummyTrustManager), new java.security.SecureRandom())
          myBuilder.setCustomSSLContext(sslContext)
          myBuilder.setEnabledSSLCiphers(Array("TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA")).setEnabledSSLProtocols(Array("TLS"))
          TLSUtils.disableHostnameVerificationForTlsCertificicates(myBuilder)
          TLSUtils.setSSLv3AndTLSOnly(myBuilder)
        }
    }
//    builder.setDebuggerEnabled(Random.nextInt(100) >= 90)
//    ReconnectionManager.setDefaultReconnectionPolicy(ReconnectionPolicy.RANDOM_INCREASING_DELAY)
//    builder.setDebuggerEnabled(true)
    builder.build()
  }


  def LoginToServer(mConnection : AbstractXMPPConnection, userID : String, userPW : String) : Either[Exception, Boolean] = try{
    mConnection.login(userID, userPW)
    Right(true)
  } catch {
    case alreadyLoggedInException : AlreadyLoggedInException =>
      Right(false)
    case notConnectedException : NotConnectedException => Left(notConnectedException)
    case e : Exception => Left(e)
  }


  def LogOutToServer(mConnection : AbstractXMPPConnection) : Either[Exception, Boolean] = try {
    mConnection.disconnect(new Presence(Presence.Type.unavailable, "", 1, Presence.Mode.away))
    Right(true)
  } catch {
    case notConnectedException : NotConnectedException =>
      Right(false)
    case e : Exception => Left(e)
  }

  def ChangePresence(mConnection : AbstractXMPPConnection, presenceType : Presence.Type, statusMsg : String): Either[Exception, Boolean] ={
    val presence = new Presence(presenceType)
    if(statusMsg != "") presence.setStatus(statusMsg)
    try{
      mConnection.sendStanza(presence)
      Right(true)
    }
    catch {
      case notConnectedException : NotConnectedException =>
        Left(notConnectedException)
    }
  }

  def ChangePresence(mConnection : AbstractXMPPConnection, presenceType : Presence.Type) : Either[Exception, Boolean] =
    try{
      ChangePresence(mConnection, presenceType, "")
      Right(true)
    }catch {
      case e : Exception=>
        Left(e)
    }

  def GetFriendList(mConnection : AbstractXMPPConnection) : List[RosterEntry] = Roster.getInstanceFor(mConnection).getEntries.toList

  def AddFriend(connection : AbstractXMPPConnection, userName : String) : Either[Exception, Boolean] = try{
    Roster.getInstanceFor(connection).createEntry(userName, userName.split("@")(0), null)
    Right(true)
  } catch {
    case notConnectedException : NotConnectedException =>
      Left(notConnectedException)
    case notLoggedInException : NotLoggedInException =>
      Left(notLoggedInException)
    case e : Exception =>
      Left(e)
  }

  def DeleteFriend(connection: AbstractXMPPConnection, userEntry : RosterEntry) : Either[Exception, Boolean] = try{
    Roster.getInstanceFor(connection).removeEntry(userEntry)
    Right(true)
  } catch {
    case notConnectedException : NotConnectedException =>
      Left(notConnectedException)
    case notLoggedInedException : NotLoggedInException =>
      Left(notLoggedInedException)
    case e : Exception =>
      Left(e)
  }


  def CreateAccount(mConnection : AbstractXMPPConnection, userID : String, userPW : String) : Either[Exception, Boolean] = {
    try{
      AccountManager.getInstance(mConnection).createAccount(userID, userPW)
      Right(true)
    } catch {
      case notConnectedException : NotConnectedException =>
        Left(notConnectedException)
      case xmppErrorException : XMPPErrorException =>
        xmppErrorException.getXMPPError.getCondition match {
          case Condition.conflict =>
            Right(false)
          case _=>
            Left(xmppErrorException)
        }
      case e : Exception =>
        Left(e)
    }
  }


  def DeleteAccount(mConnection : AbstractXMPPConnection) : Either[Exception, Boolean] = try{
    AccountManager.getInstance(mConnection).deleteAccount()
    Right(true)
  }catch {
    case notConnectedException : NotConnectedException =>
      Left(notConnectedException)
    case xmppException : XMPPErrorException =>
      Left(xmppException)
    case e : Exception =>
      Left(e)
  }
}