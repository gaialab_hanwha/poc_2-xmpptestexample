package com.gaia.XMPPClient.XMPPActor.XMPPWrapper

import akka.actor.ActorRef
import org.jivesoftware.smackx.ping.PingFailedListener

/**
 * Created by ktz on 16. 1. 21.
 */
case class CasePingFailed()
class PingFailListener(pingFailListener: ActorRef) extends PingFailedListener{
  override def pingFailed(): Unit = pingFailListener ! CasePingFailed()
}
