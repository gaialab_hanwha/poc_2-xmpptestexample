package com.gaia.XMPPClient.XMPPActor.XMPPWrapper

import org.jivesoftware.smack.SmackException.{AlreadyConnectedException, NotConnectedException}
import org.jivesoftware.smack.{AbstractXMPPConnection, XMPPConnection}
import org.jivesoftware.smack.chat.{ChatManagerListener, Chat, ChatManager, ChatMessageListener}

/**
 * Created by ktz on 15. 7. 15.
 */
trait Chater {

  def ConnectToServer(mConnection : AbstractXMPPConnection) : Either[Exception, Boolean] = try{
    if(!mConnection.isConnected) mConnection.connect()
    Right(true)
  }catch {
    case alreadyConnectedException :  AlreadyConnectedException =>
      Right(false)
    case exception : Exception =>
      Left(exception)
  }

  def SendMessage(chat : Chat, message : String) : Either[Exception, Boolean] = {
    try{
      chat.sendMessage(message)
      Right(true)
    }
    catch {
      case e : NotConnectedException => Left(e)
      case exception : Exception => Left(exception)
    }
  }

  def CreateChat(connection : XMPPConnection, userID : String, messageListener: ChatMessageListener) : Chat = {
    val chat = ChatManager.getInstanceFor(connection).createChat(userID)
    ChatManager.getInstanceFor(connection).addChatListener(new ChatManagerListener {
      override def chatCreated(chat: Chat, createdLocally: Boolean): Unit = chat.addMessageListener(messageListener)
    })
    chat
  }
}



