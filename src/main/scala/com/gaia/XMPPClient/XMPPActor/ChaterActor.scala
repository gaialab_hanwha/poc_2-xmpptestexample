package com.gaia.XMPPClient.XMPPActor

import akka.actor.{Actor, ActorLogging, ActorRef}
import com.gaia.XMPPClient.{ActorConfig, BruteMode}
import com.gaia.XMPPClient.XMPPActor.XMPPWrapper.Chater
import org.jivesoftware.smack.SmackException.NotConnectedException
import org.jivesoftware.smack.{AbstractXMPPConnection, XMPPConnection}
import org.jivesoftware.smack.chat.{ChatManagerListener, ChatManager, Chat, ChatMessageListener}
import org.jivesoftware.smack.packet.Message
import org.joda.time.DateTime
import scalaz.Scalaz._
import scala.concurrent.Future
import scala.util.Random
import scalaz.effect.IO
import scala.concurrent.duration._


/**
 * Created by ktz on 15. 7. 15.
 */
case class caseSendMessage(buddyID : String, message : String)
case class caseScheduledMessage(buddyID : String, message : String)
case class MessageReceived(chat : Chat, message : Message)
class ChaterActor(connection : AbstractXMPPConnection, mUserID : String, config: ActorConfig) extends Chater with Actor with ActorLogging  {
  import scala.collection.mutable.Map
  val mChatMap : Map[String, Chat] = Map.empty

  def RetryDuration : FiniteDuration = (scala.util.Random.nextInt(config.retryTimeOut - config.retryTimeMin) + config.retryTimeMin) millisecond

  def RequestRetryXMPPIO(xmppIO : IORequest) : Unit = {
    import context.dispatcher
    if (config.testMode == BruteMode) self ! xmppIO
    else context.system.scheduler.scheduleOnce(RetryDuration, self, xmppIO)
  }


  def RequestSendMessage(buddyID : Chat, message : String) : IO[Either[Exception, Boolean]] = IO{
    SendMessage(buddyID, message) match {
      case Right(b) =>
        Right(true)
      case Left(exception) => exception match {
        case notConnectedException : NotConnectedException =>
          ConnectToServer(connection) match {
            case Right(b) =>
              SendMessage(buddyID, message)
            case Left(e) =>
              Left(e)
          }
      }
    }
  }

  def receive = {
    case caseSendMessage(buddyID, message) =>
      val chat = GetOrCreateChat(mChatMap, buddyID)
      Future {
        RequestSendMessage(chat.get, message).unsafePerformIO() match {
          case Right(b) =>
            log.debug(s"Message Send -  $message")
          case Left(e) =>
            RequestRetryXMPPIO(RequestXMPPIO(RequestSendMessage(chat.get, message)))
        }
      }(XMPPClientActor.XMPPClientContext)
    case MessageReceived(chat, message) =>
      log.debug(s"Message Received - ${message.getBody}")
    case caseScheduledMessage(buddyID, message) =>
      context.system.scheduler.schedule((Random.nextInt(420) + 180) second, (Random.nextInt(420) + 180) second, self, caseSendMessage(buddyID, message))(context.dispatcher)
    case RequestXMPPIO(io) => Future {
      io.unsafePerformIO() match {
        case Right(b) =>
        case Left(e) =>
          RequestRetryXMPPIO(RequestXMPPIO(io))
      }
    }(XMPPClientActor.XMPPClientContext)
  }

/*  def SendMessageMode : Receive = {
    case caseSendMessage(buddyID, message) =>
      val chat = GetOrCreateChat(mChatMap, buddyID)
      Future {
        RequestSendMessage(chat.get, message).unsafePerformIO() match {
          case Right(b) =>
          case Left(e) =>
            RequestRetryXMPPIO(RequestXMPPIO(RequestSendMessage(chat.get, message)))
        }
      }(scala.concurrent.ExecutionContext.Implicits.global)
    case MessageReceived(chat, message) =>
            if (isSender) {
              if(System.currentTimeMillis() - message.getBody.split(" Sending Time - ")(1).toLong > 3000){
                log.info(s"\nMessage Returned - From ${message.getFrom.split("/")(0)} - Time : ${new DateTime(message.getBody.split(" Sending Time - ")(1).toLong).toString("HH:mm:ss.SSS")} - Received : ${DateTime.now.toString("HH:mm:ss.SSS")}")
              }
            } else {
              self ! caseSendMessage(message.getFrom.split("/")(0), message.getBody)
            }
      if(!isSender) self ! caseSendMessage(message.getFrom.split("/")(0), message.getBody)
    case RequestXMPPIO(io) => Future {
      io.unsafePerformIO() match {
        case Right(b) =>
        case Left(e) =>
          RequestRetryXMPPIO(RequestXMPPIO(io))
      }
    }(scala.concurrent.ExecutionContext.Implicits.global)
    case _=>
  }*/

  def GetOrCreateChat(chatMap : Map[String, Chat], buddyID : String) : Option[Chat] = chatMap.get(buddyID).orElse{
    val chat = CreateChat(connection, buddyID, new MyChatMessageListener(self))
    mChatMap += buddyID -> chat
    Some(chat)
  }

  class MyChatMessageListener(listener : ActorRef) extends ChatMessageListener{

    override def processMessage(chat: Chat, message: Message): Unit = {
      listener ! MessageReceived(chat, message)
    }
  }
}
