package com.gaia.XMPPClient.XMPPActor

import java.util.concurrent.{LinkedBlockingDeque, TimeUnit, ThreadPoolExecutor}

import akka.actor._
import com.gaia.XMPPClient._
import com.gaia.XMPPClient.ManagementMessage._
import com.gaia.XMPPClient.XMPPActor.XMPPWrapper._
import org.jboss.netty.handler.timeout.TimeoutException
import org.jivesoftware.smack.XMPPException.XMPPErrorException
import org.jivesoftware.smack.packet.XMPPError.Condition
import org.jivesoftware.smack.roster.{Roster, RosterEntry}
import org.jivesoftware.smack.sasl.{SASLErrorException}
import org.jivesoftware.smack.{XMPPConnection, SmackException, AbstractXMPPConnection}
import org.jivesoftware.smack.SmackException.{NotLoggedInException, NotConnectedException}
import org.jivesoftware.smack.packet.{Presence}
import org.jivesoftware.smackx.ping.PingManager
import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._
import scala.util.Random
import scalaz.effect.{IO}

/**
 * Created by ktz on 15. 7. 15.
 */
trait IORequest
case class RequestXMPPIO(iO: IO[Either[Exception, Boolean]]) extends IORequest
case class AddFriend(nOfClient : Int) extends XMPPAction
case class RequestToReconnect() extends XMPPAction
class XMPPClientActor(val StaticManager : ActorRef, val mServerName : String, val mHostAddress : String, mUserID : String, mUserPW : String, config : ActorConfig) extends XMPPClient with Actor with ActorLogging{
//  private val PING_INTERVAL = Random.nextInt(30) + 150
  private val PING_INTERVAL = -1
  private var mConnection : AbstractXMPPConnection = null
  private var mPingManager : PingManager = null
  CleanConnection()
  val mChaterActor = context.actorOf(Props(new ChaterActor(mConnection, mUserID, config)))

  /**
   * XMPP 동작을 위한 Partial Function
   */

  def RequestLogin(XMPPAction : AbstractXMPPConnection) : IO[Either[Exception, Boolean]] = IO {
    LoginToServer(mConnection, mUserID, mUserPW) match {
      case Right(notLoggedIn) =>
        ChangePresence(mConnection, Presence.Type.available)
        Right(notLoggedIn)
      case Left(exception) => exception match {
        case notConnectedException : NotConnectedException =>
          ConnectToServer(mConnection) match {
            case Right(b) =>
              LoginToServer(mConnection, mUserID, mUserPW)
            case Left(e) =>
              Left(e)
          }
        case saslError : SmackException =>
          Left(saslError)
        case exception : Exception =>
          Left(exception)
      }
    }
  }

  def RequestCreateAccount(XMPPAction : AbstractXMPPConnection) : IO[Either[Exception, Boolean]]= IO{
    CreateAccount(mConnection, mUserID, mUserPW) match {
      case Right(notConflict) =>
        Right(notConflict)
      case Left(e) => e match {
        case notConnectedException : NotConnectedException =>
          ConnectToServer(mConnection) match {
            case Right(b) =>
              CreateAccount(mConnection, mUserID, mUserPW)
            case Left(e) =>
              Left(e)
          }
        case exception : Exception =>
          Left(exception)
      }
    }
  }

  def RequestDeleteAccount(mConnection : AbstractXMPPConnection) : IO[Either[Exception, Boolean]] = IO {
    DeleteAccount(mConnection) match {
      case Right(b) =>
        LogOutToServer(mConnection)
        Right(b)
      case Left(exception) => exception match {
        case notConnectedException : NotConnectedException =>
          ConnectToServer(mConnection) match {
            case Right(b) =>
              DeleteAccount(mConnection)
            case Left(e) =>
              Left(e)
          }
        case xmppException : XMPPErrorException =>
          if(xmppException.getXMPPError.getCondition == Condition.not_allowed) LoginToServer(mConnection, mUserID, mUserPW) match {
            case Right(b) =>
              DeleteAccount(mConnection)
            case Left(e) =>
              Left(e)
          }
          else Left(xmppException)
        case exception : Exception =>
          Left(exception)
      }
    }
  }

  def RequestAddFriend(mConnection : AbstractXMPPConnection, UserName : String) : IO[Either[Exception, Boolean]] = IO {
    AddFriend(mConnection, UserName) match {
      case Right(b) =>
        Right(b)
      case Left(exception) => exception match {
        case notConnectedException: NotConnectedException =>
          ConnectToServer(mConnection) match {
            case Right(b) =>
              LoginToServer(mConnection, mUserID, mUserPW) match {
                case Right(b) =>
                  AddFriend(mConnection, UserName)
                case Left(e) =>
                  Left(e)
              }
            case Left(e) =>
              Left(e)
          }
        case notLoggedInException: NotLoggedInException =>
          LoginToServer(mConnection, mUserID, mUserPW) match {
            case Right(b) =>
              AddFriend(mConnection, UserName)
            case Left(e) =>
              Left(e)
          }
        case exception: Exception =>
          Left(exception)
      }
    }
  }

  def RequestDeleteFriend(mConnection : AbstractXMPPConnection, user : RosterEntry) : IO[Either[Exception, Boolean]] = IO{
    DeleteFriend(mConnection, user) match {
      case Right(b) =>
        Right(b)
      case Left(e) => e match {
        case notConnectedException : NotConnectedException =>
          ConnectToServer(mConnection) match {
            case Right(b) =>
              DeleteFriend(mConnection, user)
            case Left(e) =>
              Left(e)
          }
        case notLoggedInException : NotLoggedInException =>
          LoginToServer(mConnection, mUserID, mUserPW) match {
            case Right(b) =>
              DeleteFriend(mConnection, user)
            case Left(e) =>
              Left(e)
          }
        case exception : Exception =>
          Left(exception)
      }
    }
  }

  def RequestLongRunScenario() : IO[Either[Exception, Boolean]] = IO(ConnectToServer(mConnection)).flatMap(_ match {
    case Right(b) =>
      RequestLogin(mConnection).map(_ match {
        case Right(b) =>
          if(!Roster.getInstanceFor(mConnection).isLoaded) Roster.getInstanceFor(mConnection).reloadAndWait()
          if(Roster.getInstanceFor(mConnection).isLoaded) {
            mChaterActor ! caseScheduledMessage(s"${GetFriendList(mConnection).head.getName}@${mHostAddress}", s"hello I`m $mUserID You are ${GetFriendList(mConnection).head.getName}")
            Right(b)
          } else {
            Left(new TimeoutException("Roster is not reloaded yet"))
          }
        case Left(e) =>
          Left(e)
      })
    case Left(e) =>
      IO(Left(e))
  })

  def SendMessage() : IO[Either[Exception, Boolean]] = IO{
    if(!Roster.getInstanceFor(mConnection).isLoaded) Roster.getInstanceFor(mConnection).reloadAndWait()
    if(Roster.getInstanceFor(mConnection).isLoaded) {
      mChaterActor ! caseScheduledMessage(s"${GetFriendList(mConnection).head.getName}@${mHostAddress}", s"hello I`m $mUserID You are ${GetFriendList(mConnection).head.getName}")
      Right(true)
    } else {
      Left(new TimeoutException("Roster is not reloaded yet"))
    }
  }

  def ReconnectToServer(mConnection : AbstractXMPPConnection) : IO[Either[Exception, Boolean]] = IO(LogOutToServer(mConnection)).flatMap(_ => RequestLogin(mConnection))

  /**
   * XMPP Connection Listener 처리를 위한 Partial Function
   */

  def GetMySequence(userID : String) : Int = userID.split("_")(1).toInt

  def CleanConnection(): Unit = {
    if(mConnection != null && mConnection.isConnected) mConnection.disconnect(new Presence(Presence.Type.unavailable))
    mConnection = GetConnection(mServerName, mHostAddress, config.bosh)
    mConnection.addConnectionListener(XMPPConnectionListener(StaticManager, self))
    mConnection.addConnectionListener(XMPPConnectionListener(self, self))
    mConnection.setPacketReplyTimeout(config.packetReplyTimeOut)
//    PingManager.getInstanceFor(mConnection).setPingInterval(-1)
    mPingManager = PingManager.getInstanceFor(mConnection)
    mPingManager.setPingInterval(PING_INTERVAL)
    mPingManager.registerPingFailedListener(new PingFailListener(self))
  }

  def RetryDuration : FiniteDuration = (scala.util.Random.nextInt(config.retryTimeOut - config.retryTimeMin) + config.retryTimeMin) millisecond

  def RequestRetryXMPPIO(xmppIO : IORequest) : Unit = {
    import context.dispatcher
    if (config.testMode == BruteMode) self ! xmppIO
    else context.system.scheduler.scheduleOnce(RetryDuration, self, xmppIO)
  }


  def receive = {
    case MsgEnvelope(from, action) =>
//      import scala.concurrent.ExecutionContext.Implicits.global
      import XMPPClientActor.XMPPClientContext
      action match {
        case RequestLogInAll() => Future{
          if(config.testMode == NormalMode) {
            context.system.scheduler.scheduleOnce(config.logIngSeqTime * GetMySequence(mUserID) millisecond, self, RequestXMPPIO(RequestLogin(mConnection)))(context.dispatcher)
          }
          else {
            RequestLogin(mConnection).unsafePerformIO() match {
              case Right(b) =>
              case Left(e) => e match {
                case saslErrorException : SASLErrorException =>
                  log.error(saslErrorException, saslErrorException.getMessage)
                case exception : Exception =>
                  log.error(s"Login Failed : ${exception.toString}")
                  RequestRetryXMPPIO(RequestXMPPIO(RequestLogin(mConnection)))
              }
            }
          }
        }
        case RequestCreateAccountAll() => Future{
          if(config.testMode == NormalMode) context.system.scheduler.scheduleOnce(config.logIngSeqTime * GetMySequence(mUserID) millisecond, self, RequestXMPPIO(RequestCreateAccount(mConnection)))(context.dispatcher)
          else {
            RequestCreateAccount(mConnection).unsafePerformIO() match {
              case Right(b) =>
              case Left(e) =>
                RequestRetryXMPPIO(RequestXMPPIO(RequestCreateAccount(mConnection)))
            }
          }
        }
        case RequestDeleteAccountAll() => Future{
          RequestDeleteAccount(mConnection).unsafePerformIO() match {
            case Right(b) =>
            case Left(e) =>
              RequestRetryXMPPIO(RequestXMPPIO(RequestDeleteAccount(mConnection)))
          }
        }
        case RequestLogOutAll() => Future {
          LogOutToServer(mConnection) match {
            case Right(b) =>
            case Left(e) =>
              RequestRetryXMPPIO(RequestXMPPIO(IO(LogOutToServer(mConnection))))
          }
        }
        case AddFriend(nOfClient) => Future{
          val range : List[String]= if(mUserID.split("_")(0).toInt % 2 == 0) List(s"${(mUserID.split("_")(0).toInt + 1) % nOfClient}_${GetMySequence(mUserID)}@$mHostAddress") else List(s"${(mUserID.split("_")(0).toInt + - 1) % nOfClient}_${GetMySequence(mUserID)}@$mHostAddress")
          range foreach { UserName =>
            RequestAddFriend(mConnection, UserName).unsafePerformIO() match {
              case Right(b) =>
              case Left(e) => RequestRetryXMPPIO(RequestXMPPIO(RequestAddFriend(mConnection, UserName)))
            }
          }
        }
        case RequestDeleteFriendEachOther() => Future {
          GetFriendList(mConnection) foreach { user =>
            RequestDeleteFriend(mConnection, user).unsafePerformIO() match {
              case Right(b) =>
              case Left(e) =>
                RequestRetryXMPPIO(RequestXMPPIO(RequestDeleteFriend(mConnection, user)))
            }
          }
        }
        case RequestToReconnect() => Future{
          ReconnectToServer(mConnection).unsafePerformIO() match {
            case Right(b) =>
              log.debug("Connection Reconnecting...")
            case Left(e) =>
              RequestRetryXMPPIO(RequestXMPPIO(RequestLogin(mConnection)))
          }
        }
        case RequestLongRun() => Future {
          RequestLongRunScenario().unsafePerformIO() match {
            case Right(b) =>
            case Left(e) =>
              RequestRetryXMPPIO(RequestXMPPIO(RequestLongRunScenario()))
          }
        }
        case RequestSendMessage() => if(GetMySequence(mUserID) % 2 == 0) {
          Future {
            SendMessage().unsafePerformIO() match {
              case Right(b) =>
              case Left(e) =>
                RequestRetryXMPPIO(RequestXMPPIO(SendMessage()))
            }
          }
        }
      }
    case RequestXMPPIO(xmppIO) => Future {
      xmppIO.unsafePerformIO() match {
        case Right(b) =>
        case Left(e) =>
          RequestRetryXMPPIO(RequestXMPPIO(xmppIO))
      }
    }(XMPPClientActor.XMPPClientContext)
    case ConnectionClosedOnError(e) =>
      log.error(s"Connection Closed On Error : ${e.getMessage}")
      self ! MsgEnvelope(self, RequestLogInAll())
    case ConnectionClosed() =>
      log.error("Connection Closed")
      self ! MsgEnvelope(self, RequestLogInAll())
    case ConnectionAuthenticated(connection, resumed) =>
    case CasePingFailed() =>
      log.error(s"Ping Failed! ${mConnection.getUser}")
//      mPingManager.setPingInterval(PING_INTERVAL)
//      mPingManager.pingMyServer()
  }
}

object XMPPClientActor {
  implicit val XMPPClientContext : ExecutionContext = ExecutionContext.fromExecutor(new ThreadPoolExecutor(50, 1000, 60, TimeUnit.SECONDS, new LinkedBlockingDeque[Runnable]()))
  def apply(staticActor : ActorRef, mServerName : String, mHostAddress : String, mUserID : String, mUserPW : String, config : ActorConfig) : Props =
    Props(new XMPPClientActor(staticActor, mServerName, mHostAddress, mUserID, mUserPW, config))
}