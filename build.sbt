name := "XMPPServerLoadTest"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= {
  val Akkav = "2.3.14"
  val SmackV = "4.1.3"
  val ScalazV = "7.1.4"
  Seq(
    "org.scalaz" %% "scalaz-core" % ScalazV,
    "org.scalaz" %% "scalaz-effect" % ScalazV,
    "com.typesafe.akka" %% "akka-actor" % Akkav,
    "com.typesafe.akka" %% "akka-remote" % Akkav,
    "com.typesafe.akka" %% "akka-slf4j" % Akkav,
//    "org.igniterealtime.smack" % "smack-tcp" % SmackV,
//    "org.igniterealtime.smack" % "smack-bosh" % SmackV,
//    "org.igniterealtime.smack" % "smack-java7" % SmackV,
//    "org.igniterealtime.smack" % "smack-im" % SmackV,
//    "org.igniterealtime.smack" % "smack-extensions" % SmackV,
    "org.igniterealtime.jbosh" % "jbosh" % "0.8.0",
    "com.typesafe.slick" %% "slick" % "3.0.2",
    "mysql" % "mysql-connector-java" % "5.1.6",
    "org.slf4j" % "slf4j-nop" % "1.6.4",
    "com.zaxxer" % "HikariCP" % "2.4.1",
    "com.github.nscala-time" %% "nscala-time" % "2.2.0",
    "xpp3" % "xpp3" % "1.1.4c",
    "org.jxmpp" % "jxmpp-core" % "0.4.2-beta1"
  )
}
unmanagedJars in Compile += file("lib/smack-bosh-4.1.3.jar")
unmanagedJars in Compile += file("lib/smack-core-4.1.3.jar")
unmanagedJars in Compile += file("lib/smack-extensions-4.1.3.jar")
unmanagedJars in Compile += file("lib/smack-im-4.1.3.jar")
unmanagedJars in Compile += file("lib/smack-java7-4.1.3.jar")
unmanagedJars in Compile += file("lib/smack-tcp-4.1.3.jar")